"use strict";
define([
        "esri/toolbars/navigation",

        // For emitting events
        "dojo/Evented",

        // needed to create a class
        "dojo/_base/declare",
        "dojo/_base/lang",

        // widget class
        "dijit/_WidgetBase",

        // accessibility click
        "dijit/a11yclick",

        // templated widget
        "dijit/_TemplatedMixin",

        // handle events
        "dojo/on",

        // load template
        "dojo/text!elevation-navigation-js/templates/navigation.html",

        // localization
        "dojo/i18n!elevation-navigation-js/nls/navigation",

        // dom manipulation
        "dojo/dom-class",

        // wait for dom to be ready
        "dojo/domReady!"
    ],

    function(Navigation, Evented, declare, lang, _WidgetBase, a11yclick,
        _TemplatedMixin, on, dijitTemplate, i18n, domClass) {
        return declare("elevation-navigation-js.Navigation", [_WidgetBase, _TemplatedMixin, Evented], {

            templateString: dijitTemplate,

            options: {
                map: null
            },

            constructor: function(options, srcRefNode) {
                // css classes
                this.css = {
                    navigation: "navigation",
                    zoomin: "zoomin",
                    zoomout: "zoomout",
                    pan: "pan",
                    previous: "previous",
                    next: "next",
                    container: "container"
                };

                // language
                this._i18n = i18n;

                // mix in settings and defaults
                var defaults = lang.mixin({}, this.options, options);

                // create the DOM for this widget
                this.domNode = srcRefNode;

                // set properties
                this.set("map", defaults.map);
                this.set("activeTool", Navigation.PAN);
            },

            postCreate: function() {
                // own this accessible click event button
                // Custom press, release, and click synthetic events which trigger on a left mouse click, touch, or space/enter keyup.
                this.own(on(this.zoominNode, a11yclick, lang.hitch(this, this._toggle_zoomin)));
                this.own(on(this.zoomoutNode, a11yclick, lang.hitch(this, this._toggle_zoomout)));
                this.own(on(this.panNode, a11yclick, lang.hitch(this, this._toggle_pan)));
                this.own(on(this.prevNode, a11yclick, lang.hitch(this, this._click_prev)));
                this.own(on(this.nextNode, a11yclick, lang.hitch(this, this._click_next)));
            },

            startup: function() {
                on.once(this.map, "load", lang.hitch(this, function() {

                    this.set("nav", new Navigation(this.map));

                    this.set("loaded", true);

                    // emit event
                    this.emit("load", {});

                }));
            },

            destroy: function() {
                // call the superclass method of the same name.
                this.inherited(arguments);
            },

            _toggle_zoomin: function() {
                domClass.toggle(this.zoominNode, "activated");
                domClass.remove(this.zoomoutNode, "activated");
                domClass.remove(this.panNode, "activated");
                if (this.activeTool == Navigation.ZOOM_IN) {
                    this.nav.deactivate();
                    return;
                }
                this.nav.activate(Navigation.ZOOM_IN);
                this.activeTool = Navigation.ZOOM_IN;
            },

            _toggle_zoomout: function() {
                domClass.toggle(this.zoomoutNode, "activated");
                domClass.remove(this.zoominNode, "activated");
                domClass.remove(this.panNode, "activated");
                if (this.activeTool == Navigation.ZOOM_OUT) {
                    this.nav.deactivate();
                    return;
                }
                this.nav.activate(Navigation.ZOOM_OUT);
                this.activeTool = Navigation.ZOOM_OUT;
            },

            _toggle_pan: function() {
                domClass.toggle(this.panNode, "activated");
                domClass.remove(this.zoominNode, "activated");
                domClass.remove(this.zoomoutNode, "activated");
                if (this.activeTool == Navigation.PAN) {
                    this.nav.deactivate();
                    return;
                }
                this.nav.activate(Navigation.PAN);
                this.activeTool = Navigation.PAN;
            },

            _click_prev: function() {
                this.nav.zoomToPrevExtent();
            },

            _click_next: function() {
                this.nav.zoomToNextExtent();
            }
        });
    });
