define({
    root: ({
        navigation: {
            "zoomin": {
                "title": "Zoom in",
                "tooltip": "Zoom in to the map"
            },
            "zoomout": {
                "title": "Zoom out",
                "tooltip": "Zoom out from the map"
            },
            "pan": {
                "title": "Pan",
                "tooltip": "Pan the map"
            },
            "previous": {
                "title": "Previous extent",
                "tooltip": "Zoom to previous extent"
            },
            "next": {
                "title": "Next extent",
                "tooltip": "Zoom to next extent"
            }
        }
    }),
    "ar": 1,
    "da": 1,
    "de": 1,
    "es": 1,
    "et": 1,
    "fr": 1,
    "he": 1,
    "it": 1,
    "ja": 1,
    "ko": 1,
    "lv": 1,
    "lt": 1,
    "nl": 1,
    "nb": 1,
    "pl": 1,
    "pt-br": 1,
    "pt-pt": 1,
    "ro": 1,
    "ru": 1,
    "sv": 1,
    "zh": 1,
    "zh-cn": 1
});
